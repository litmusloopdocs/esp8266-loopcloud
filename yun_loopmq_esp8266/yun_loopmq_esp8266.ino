/*
 Litmus Loop
 */

#include <ArduinoJson.h>
#include <loopmq.h>
#include "ESP8266WiFi.h"
#include "configuration.h"

/*
Passing the defined information
*/

const char* ssid = network_ssid;              // WiFi Network key
const char* password = network_password;      // WiFi Network password
int port = port_number;                       // Port Number
char servername[] = server;                   // Server name
char c[15] = clientID;                        // ClientID
char pass[16]= passkey;                       // password
char user[16] = userID;                       // username
char p[]= subTOPIC;                           // Subscribe on this topic to get the data
char s[]= pubTopic;                           // Publish on this topic to send data or command to device  


WiFiClient esp8266;                           // Client defination 
PubSubClient loopmq(esp8266);                 // instance to use the loopmq functions.


String relayStatus;               
  
                                                              
String relaystatus(){                         // Function to find the relay Status
if(digitalRead(5) == HIGH)
return relayStatus = "ON";
else if (digitalRead(5) == LOW)
return relayStatus = "OFF";
else
return relayStatus = "NOT WORKING";
}

void callback(char* topic, byte* payload, unsigned int length) {
 Serial.print("Message arrived [");
 Serial.print(topic);
 Serial.print("] ");
  
/*
Display the message published serially and on the LCD
*/
  for (int i=0;i<length;i++) {
    Serial.print((char)payload[i]);           // Serial Display
  }
     Serial.println();                        //Print on a new line
}

void reconnect() {                        
   
  while (!loopmq.connected()) {               // Loop until we're reconnected
  Serial.print("Attempting MQTT connection...");     
    if (loopmq.connect(c, user, pass)) {                  // Attempt to connect
    Serial.println("connected");              // Once connected, publish an announcement...
         
         loopmq.subscribe(s);                 // Subscribe to a topic  
/*  
To Unscubscribe from a Topic uncomment the code below   
*/
        
   // loopmq.unsubscribe(s); 
   
      /*
To disconnect unocmment the code below
*/
      
   //  loopmq.disconnect();            
    } 
    else {
   Serial.print("failed, rc=");
   Serial.print(loopmq.state());
   Serial.println(" try again in 5 seconds");
      
      delay(5000);                            // Wait 5 seconds before retrying
    }
  }
}




void setup(void)
{ 

  pinMode(5, OUTPUT);                                           // Define the output GPIO 5
  pinMode(0, INPUT);                                            // button GPIO 0


Serial.begin(115200);                                           // Start Serial
  

WiFi.begin(ssid, password);                                    // Connect to WiFi
while (WiFi.status() != WL_CONNECTED) {
delay(500);
Serial.print(".");
}
Serial.println("");
Serial.println("WiFi connected");

  loopmq.setServer(servername, port);             // Connect to the specified server and port defined by the user
  loopmq.setCallback(callback);               // Call the callbeck funciton when published  


}

void loop() {

/*
 Sensor Code
 */
 if(digitalRead(0) == HIGH){
  digitalWrite(5, LOW);                // turn the LED on (HIGH is the voltage level)
  Serial.println(relaystatus());
  delay(1000);                          // wait for a second
 }
 else{
  digitalWrite(5, HIGH);                 // turn the LED off by making the voltage LOW
  Serial.println(relaystatus());
  delay(1000);                         // wait for a second
 }
  /*  
  JSON parser
  */
  StaticJsonBuffer<200> jsonBuffer;                   //  Inside the brackets, 200 is the size of the pool in bytes.If the JSON object is more complex, you need to increase that value. 

  JsonObject& root = jsonBuffer.createObject();      // It's a reference to the JsonObject, the actual bytes are inside the JsonBuffer with all the other nodes of the object tree.
  root["command"] = "ESP8266 EVB on board relay";    // Add values in the object, add the objects you want to add to the JSON in the form of root["key'] = value;


  JsonObject& data= root.createNestedObject("data"); // nested JSON 
  data["Relay Status"]=relaystatus();


  root.printTo(Serial);                              // prints to serial terminal
  Serial.println();

  char buffer[200];                                 // buffer to pass as payload
  root.printTo(buffer, sizeof(buffer));             // copy the JSON to the buffer to pass as a payload 
  /*
  Publish to the server
  */

  if (!loopmq.connected()) {
    reconnect();                                    // Try to reconnect if connection dropped
  }
  if (loopmq.connect(c, user, pass)){
 
   loopmq.publish(p,buffer);                        // Publish message to the server 
    // delay(100);
  }
  
  loopmq.loop();                                   // check if the network is connected and also if the client is up and running

}
