# yun-loopmq-esp8266

This project is to demonstrate the implementation of MQTT. **Message Queue Telemetry Transport (MQTT)** is an extremely simple and lightweight messaging protocol, designed for constrained devices and low bandwidth, high latency and unreliable networks. The protocol uses **publish/subscribe** communication pattern and is used for machine to machine communication and plays and important role in the internet of things. MQTT works on the **TCP/IP connection**.

The **ESP8266** WiFi Module is a self contained SOC with integrated TCP/IP protocol stack that can give any microcontroller access to your WiFi network. The ESP8266 is capable of either hosting an application or offloading all Wi-Fi networking functions from another application processor. Each ESP8266 module comes pre-programmed with an AT command set firmware, meaning, you can simply hook this up to your Arduino device and get about as much WiFi-ability as a WiFi Shield offers. 

## Overview


The library provides an example of publish and subscribe messaging with a server that supports MQTT using ESP8266.
 
***Features provided by the client library:***

* Connect the device to any IP network using Ethernet, Wi-Fi, 4G/LTE
* Publish any message to the MQTT server in standard JSON format on a specific topic 
* Subscribe data from the server to the device on a specific topic
* Unsubscribe the topic to no longer communicate to the device
* Disconnect the device from any network connectivity.


***The following Table shows status of the client and the server when the above functions are implemented:***

> |Function |  Server Status   |    Client Status
> ----------------|------------------|---------------
|Looopmq.connect | Connected       | Connected|
|Loopmq.publish |Connected |  Connected|
|Loopmq.subscribe | Connected | Connected|
|Loopmq.unsubscribe | Connected | Disconnected|
|Loopmq.disconnect |  Disconnected |  Disconnected|


## Getting Started With ESP8266


**Basic steps to connect ESP8266 to internet are given below:**

>1. Connect the device to the computer using USB to Serial cable type F.
2. Define the network ssid and the paasword in the configuration file.
3. Press the Button and connect the 5V power supply and release the button to enter the bootloader mode.
4. The code can only be uploaded in bootloder mode and hence need to perform the above step every time you want to upload a code to the device.
5. Setting for the board should be as shown below
![alt text](https://bytebucket.org/litmusloopdocs/esp8266-loopcloud/raw/master/extras/board_setting.png)


**Steps to connect the board and then send data to MQTT are as below:**

>1. Assemble and connect the board as shown 
![alt text](https://bytebucket.org/litmusloopdocs/esp8266-loopcloud/raw/master/extras/initial_setup.png)
2. Install Arduino IDE and select ESP8266 from boards, if not found download it from the Boards Manager
3. Select the COM port it is connected to
3. Install the library or open the  *yun_loopmq_esp8266.ino* file from the examples.
4. Once code is compiled and flashed to you device, you should see the messages published by you to the device. 


**Steps to test MQTT connection (if required) can be found under /repo/extras/testmqqt.md :**
***Note***: *If you are not using Google Chrome as your default browser, download **MQTTSpy** to test MQTT connection.*


## Configuration

The user need to define a list of parameters in order to connect the device to a server in a secured manner.

**Below are the list of minimum definitions required by the user to send data to the cloud:**

```
#define network_ssid "LitmusAutomation-Internal"      // Network SSID
#define	network_password "Zxcvbn<>890"                // Network password
#define port_number 1883                              // Port number
#define server "iot.eclipse.org"                      // Server name
#define clientID "esp8266"                            // ClientID
#define passkey "password"                            // password
#define userID "admin"                                // username 
#define subTOPIC "esp8266/pub"                        // Subscribe on this topic to get the data
#define pubTopic "esp8266/sub"                        // Publish on this tpoic to send data or command to device 

```

## Functions

1.***loopmq.connect (client ID)***

This function is used to connect the device or the client to the client ID specified by the user.

```
if (loopmq.connect(c)) {
      Serial.println ("connected");
```       


2.***loopmq.connect (client ID, username, password)***

Checks for the username and password specified by the user to connect the device to the network.

```
if (loopmq.connect(c, user, pass))
  loopmq.publish(p,buffer);      // Publish message to the server once only
```
3.***loopmq.publish (topic, data)***

This function is used to publish data in string format to the topic specified by the user. 
In this example a counter from *1 to 100* is passed to the topic *litmus* specified by the user.

```
Loopmq.publish (p,buffer);        // Publish message to the server
```

4.***loopmq.subscribe (topic)***

This function is used to subscribe to a topic to which data will be published from the user to the     device. 

```
loopmq.subscribe(s);                    // Subscribe to a topic
```

5.***loopmq.unsubscribe (topic)***

This function is used to unsubscribe the device from the server. Calling this function will stop sending messages from the device to the server.

```
// loopmq.unsubscribe(s);   Note: uncomment the code to unsubscribe from the topic
```
 
6.***lopmq.disconnect ()***

This function is used to disconnect the device from the server. Disconnect does not stop the functionality of the device but disconnects it from the network. The device works fine locally but does not send any update to the internet.

```
// loopmq.disconnect();  Note: uncomment the code to disconnect the device
```
7.***loop ()***

This function is the sensor code for the ESP8266.

```
 if(digitalRead(0) == HIGH){
  digitalWrite(5, LOW);                // turn the LED on (HIGH is the voltage level)
  Serial.println(relaystatus());
  delay(1000);                          // wait for a second
 }
 else{
  digitalWrite(5, HIGH);                 // turn the LED off by making the voltage LOW
  Serial.println(relaystatus());
  delay(1000);                         // wait for a second
 }
```

8.***JSON PARSER***

This function is used to create a JSON payload to be passed to the broker as payload. Please refer the [link](https://github.com/bblanchon/ArduinoJson/wiki/Compatibility-issues) for any compalibility issues.

```
  StaticJsonBuffer<200> jsonBuffer;                  //Inside the brackets, 200 is the size of the pool in bytes.If the JSON object is more complex, you need to increase the size 

  JsonObject& root = jsonBuffer.createObject();      // It's a reference to the JsonObject, the actual bytes are inside the JsonBuffer with all the other nodes of the object tree.
  root["command"] = "ESP8266 EVB on board relay";    // Add values in the object, add the objects you want to add to the JSON in the form of root["key'] = value;


  JsonObject& data= root.createNestedObject("data"); // nested JSON 
  data["Relay Status"]=relaystatus();


  root.printTo(Serial);                              // prints to serial terminal
  Serial.println();

  char buffer[200];                                 // buffer to pass as payload
  root.printTo(buffer, sizeof(buffer));             // copy the JSON to the buffer to pass as a payload 

```